<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\Network\Http\Client;
use Cake\ORM\TableRegistry;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{     public function index()
     {
        $this->set('users', $this->Users->find('all'));
    }

    public function view($id)
    {
        $user = $this->Users->get($id);
        $this->set(compact('user'));
    }

    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('Unable to add the user.'));
        }
        $this->set('user', $user);
    }
    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow([
            'add', 
            'logout',
            'oauthLogin',
            'oauthOffice365Callback',
            ]);
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            //debug($user); die();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
    
    public function oauthLogin()
	{
		$oauth_config = Configure::read('oAuth');
		
		// Scope
		$scope = '';//'https://graph.microsoft.com/';
		
		// Callback URL
		$callback = Router::url(['action'=>'oauthOffice365Callback'], true);
		
		// Redirect to login page of Office365
		$url = sprintf('https://www.facebook.com/dialog/oauth/',
			$oauth_config['Facebook']['client_id'],
			urlencode($scope),
			urlencode($callback)
		);
		return $this->redirect($url);
	}
	
	public function oauthOffice365Callback()
	{
		$oauth_config = Configure::read('oAuth');
		
		// Callback URL
		$callback = Router::url(['action'=>'oauthOffice365Callback'], true);
		
		// Fetch URL params
		$code = $this->request->query('code');
		
		// Scope
		$scope = ''//'https://graph.microsoft.com/user.read.all';
		
		// Fetch access token
		$http = new Client();
		$response = $http->post('https://graph.facebook.com/oauth/access_token', [
			'code' => $code,
			'client_id' => $oauth_config['Facebook']['client_id'],
			'client_secret' => $oauth_config['Facebook']['client_secret'],
			'redirect_uri' => $callback,
			'grant_type' => 'authorization_code',
			//'scope' => urlencode($scope)
		]);
		$json_token = json_decode($response->body);
		debug($json_token);
		
		// Fetch user's data
		$http = new Client([
			'headers' => [
				'Authorization' => $json_token->token_type . ' ' . $json_token->access_token,
				'Content-Type' => 'application/json;odata.metadata=minimal;odata.streaming=true',
			]
		]);
		$response = $http->get('https://graph.microsoft.com/v1.0/me');
		$response = json_decode($response->body);
		// debug($response);
		
		$userInfo = [
			'oauth_provider' => 'Facebook',
			'oauth_id' => $response->id,
		]; // debug($userInfo);
		
		// Check the existing user
		$existingUser = $this->Users->find()->where(['oauth_id' => $userInfo['oauth_id']]);
		
		// If exists
		if ($existingUser->count()) {
			$user_id = $existingUser->select('id')->first()->toArray();
			$user_id = $user_id['id'];
		}
		
		// If not existing
		else {
			$userInfo['email'] = $response->mail;
			$userInfo['lastname'] = $response->surname;
			$userInfo['firstname'] = $response->givenName;
			$userInfo['password'] = 'OAuth';
			$userInfo['group_id'] = '2'; // Minimum Permission Group
			
			// Add user
			$user = $this->Users->newEntity();
			$user = $this->Users->patchEntity($user, $userInfo);
			$user_id = $this->Users->save($user);
			$user_id = $user_id->id;
		}
		
		// Login
		$userInfo = $this->Users->find()->where(['id' => $user_id])->first()->toArray();
		$this->Auth->setUser($userInfo);
		// debug($userInfo);die;
		
		// Redirect
		return $this->redirect($this->Auth->redirectUrl());
	}
}
